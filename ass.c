/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#include <bits/sockaddr.h>
#include <asm/types.h>
#include <linux/rtnetlink.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

/* del == 1 - remove ip, del == 0 - add */
int change_ip(const char *ip, int mask, const char *iface, int del) {
    int ret = 0; /* error */

    int fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
    if(-1 == fd) {
        fprintf(stderr, "socket() failed: %s\n", strerror(errno));
        goto cleanup;
    }

    /* setup local address & bind using this address */
    struct sockaddr_nl la = { 0 };
    la.nl_family = AF_NETLINK;
    la.nl_pid = getpid();
    if(-1 == bind(fd, (struct sockaddr*) &la, sizeof(la))) {
        fprintf(stderr, "bind() failed: %s\n", strerror(errno));
        goto cleanup;
    }

    /* initalize RTNETLINK request buffer */
    struct {
        struct nlmsghdr nl;
        struct ifaddrmsg rt;
        char buf[8192];
    } req = { 0 };

    /* compute the initial length of the service request */
    size_t ifal = sizeof(struct ifaddrmsg);
    /*
     * add first attrib:
     * set IP addr
     * RTNETLINK buffer size
     */
    struct rtattr *rtap = (struct rtattr *) req.buf;
    rtap->rta_type = IFA_ADDRESS;
    rtap->rta_len = sizeof(struct rtattr) + 4;
    int pton_res = inet_pton(AF_INET, ip, ((char *)rtap) + sizeof(struct rtattr));
    if(0 == pton_res) {
        fprintf(stderr, "'%s' is not a valid addr\n", ip);
        goto cleanup;
    }
    else if(-1 == pton_res) {
        fprintf(stderr, "inet_pton() failed: %s\n", strerror(errno));
        goto cleanup;
    }

    ifal += rtap->rta_len;

    /*
     * add second attrib:
     * set ifc index and increment the size
     */
    rtap = (struct rtattr *) (((char *)rtap) + rtap->rta_len);
    rtap->rta_type = IFA_LOCAL;
    rtap->rta_len = sizeof(struct rtattr) + 4;
    pton_res = inet_pton(AF_INET, ip, ((char *)rtap) + sizeof(struct rtattr));
    if(0 == pton_res) {
        fprintf(stderr, "'%s' is not a valid addr\n", ip);
        goto cleanup;
    }
    else if(-1 == pton_res) {
        fprintf(stderr, "inet_pton() failed: %s\n", strerror(errno));
        goto cleanup;
    }
    ifal += rtap->rta_len;

    /* setup the NETLINK header */
    req.nl.nlmsg_len = NLMSG_LENGTH(ifal);
    req.nl.nlmsg_flags = NLM_F_REQUEST | NLM_F_CREATE | NLM_F_APPEND;
    req.nl.nlmsg_type = del ? RTM_DELADDR : RTM_NEWADDR;

    /* setup the service header (struct rtmsg) */
    req.rt.ifa_family = AF_INET;
    req.rt.ifa_prefixlen = mask;
    req.rt.ifa_flags = IFA_F_PERMANENT;
    req.rt.ifa_scope = RT_SCOPE_HOST;
    req.rt.ifa_index = if_nametoindex(iface);
    if(0 == req.rt.ifa_index) {
        fprintf(stderr, "if_nametoindex() failed: %s\n", strerror(errno));
        goto cleanup;
    }

    /* create the remote address to communicate */
    struct sockaddr_nl pa = { 0 };
    pa.nl_family = AF_NETLINK;

    /* initialize & create the struct msghdr supplied to the sendmsg() function */
    struct msghdr msg = { 0 };
    msg.msg_name = (void *) &pa;
    msg.msg_namelen = sizeof(pa);

    /* place the pointer & size of the RTNETLINK message in the struct msghdr */
    struct iovec iov = { 0 };
    iov.iov_base = (void *) &req.nl;
    iov.iov_len = req.nl.nlmsg_len;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    /* send the RTNETLINK message to kernel */
    ssize_t rtn = sendmsg(fd, &msg, 0);
    if(-1 == rtn) {
        fprintf(stderr, "sendmsg() failed: %s\n", strerror(errno));
        goto cleanup;
    }

    /* buffer to hold the RTNETLINK reply(ies) */
    char buf[8192];
    printf("Receiving reply...\n");
    rtn = recv(fd, buf, sizeof(buf), MSG_DONTWAIT|MSG_PEEK);
    if(rtn < 0 && EAGAIN == errno) {
        printf("Nothing to receive\n");
    }
    else if(rtn < 0) {
        fprintf(stderr, "recv() failed: %s\n", strerror(errno));
        goto cleanup;
    }
    else {
        printf("Received %ld bytes\n", rtn);
        struct nlmsghdr *h;
        for(h = ( struct nlmsghdr * )buf; NLMSG_OK(h, rtn); h = NLMSG_NEXT(h,ret)) {
            if(h->nlmsg_type == NLMSG_ERROR) {
                struct nlmsgerr *err = ( struct nlmsgerr* ) NLMSG_DATA( h );
                fprintf(stderr, "Received error: %s\n", strerror(-err->error));
                goto cleanup;
            }
        }
    }

    ret = 1; /* success */

cleanup:
    if(-1 != fd) {
        close(fd);
    }
    return ret;
}

int main(void) {
    int r = 0;
    const unsigned int nsecs = 3;
    printf("Assigning '127.0.0.3/24' to lo\n");
    r = change_ip("127.0.0.3", 24, "lo", 0);
    if(!r) {
        printf("Failure!\n");
        return 1;
    }
    printf("Sleeping for %u sec\n", nsecs);
    sleep(nsecs);
    printf("Removing '127.0.0.9/24' from eth3\n");
    r = change_ip("127.0.0.9", 24, "eth3", 1);
    if(r) {
        printf("Strange: success\n");
        return 1;
    }
    printf("Removing 127.0.0.3/8 from lo\n");
    r = change_ip("127.0.0.3", 8, "lo", 1);
    if(r) {
        printf("Strange: success!\n");
        return 1;
    }

    printf("Removing 127.0.0.3/24 from lo\n");
    r = change_ip("127.0.0.3", 24, "lo", 1);
    if(!r) {
        printf("Failure!\n");
        return 1;
    }

    return 0;
}
